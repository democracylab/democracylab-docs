// See https://docusaurus.io/docs/site-config for all the possible
// site configuration options.

const siteConfig = {
  title: "DemocracyLab",
  tagline: "Documentation",
  url: "https://docs.democracylab.io",
  baseUrl: "/",
  projectName: "democracylab-docs",
  organizationName: "SMU",
  headerLinks: [{ doc: "welcome", label: "Documentation" }],
  headerIcon: "",
  footerIcon: "img/thedemocracylab.png",
  favicon: "img/thedemocracylab.png",
  colors: {
    primaryColor: "#30333b",
    secondaryColor: "#9baab5"
  },
  copyright: `Copyright © ${new Date().getFullYear()} SMU`,
  highlight: {
    theme: "default"
  },
  scripts: ["https://buttons.github.io/buttons.js"],
  onPageNav: "separate",
  cleanUrl: true,
  ogImage: "img/undraw_online.svg",
  twitterImage: "img/undraw_tweetstorm.svg",
  enableUpdateTime: true,
  docsSideNavCollapsible: false
};

module.exports = siteConfig;
