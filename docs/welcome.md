---
id: welcome
title: Democracy Lab Project
sidebar_label: Welcome
---

![diagram](https://assets.democracylab.io/img/infrastructure.png)

## Where is everything?

[api.democracylab.io](https://api.democracylab.io) | The API endpoint (you probably want this)  
[status.democracylab.io](https://status.democracylab.io) | Status of the cloud  
[docs.democracylab.io](https://docs.democracylab.io) | This documentation  
[transparentparliament.com](https://transparentparliament.com) | Website for Transparent Parliament  
[democracylab.io](http://democracylab.io) | Website for Democracylab  
[assets.democracylab.io](http://assets.democracylab.io) | Asset server (Wasabi S3)

Almost everything is hosted on Digital Ocean via [PlaceCloud](https://docs.theplacelab.com/placecloud/), which is Docker. There is a [status panel](http://status.democracylab.io) if you're wondering if anything is down.

## How do I use this?

The intention is that you access the data [via the api](/docs/api).

## How do I access the servers/database directly?

If you are producing visualizations you should not need this. [Use the API](/docs/api). If you run into a case the API can't handle [let us know](https://github.com/SouthernMethodistUniversity/Democracy-Lab/issues/new) and we'll fix it! If you are a developer affiliated with SMU and really need the access, first make sure [you have an OpenVPN client installed properly](/docs/vpn_setup) and then contact Andrew or Steph via the slack to get access.

## Where are...

- The people: [Slack: smu-text-mining.slack.com](http://smu-text-mining.slack.com)
- Credentials: 1Password
- Full hansard TSV files: available on M2
- SSL Certs: Issued by Let's Encrypt, handled by acme.sh
- [Visualization code](https://github.com/SouthernMethodistUniversity/Democracy-Lab)
- [DNS for this project](https://namecheap.com)
- [Sourcecode for this documentation](https://gitlab.com/democracylab/democracylab-docs)
- [Sourcecode for democracylab cloud](https://gitlab.com/democracylab/democracylab-cloud)
- [Sourcecode for democracylab database/api](https://gitlab.com/democracylab/democracylab-api)
