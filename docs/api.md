---
id: api
title: Hansard API
sidebar_label: Hansard API
---

RESTful JSON API via PostgREST.

- Example: How to [use the database/API with R](/docs/api_with_r)
- Structure of the [database](/docs/database)
- [complete documentation of the grammar](http://postgrest.org/en/v5.2/api.html) for formulating queries

# Valid Endpoints

> Endpoints: `debate`, `text`

You formulate URLs as follows:
`BASEURL + ENDPOINT + QUERY`

BASEURL: `https://api.democracylab.io/hansard/`  
ENDPOINT: `debate`  
QUERY:`?id=eq.2`

Valid Request: `https://api.democracylab.io/hansard/debate?id=eq.2`

# Examples:

> **Given an ID, give me a single full debate**  
> Call: [https://api.democracylab.io/hansard/debate?id=eq.2](https://api.democracylab.io/hansard/debate?id=eq.2)

Returns:

```json
[
  {
    "id": 2,
    "debate_section": "0",
    "debate_sentence": "0",
    "debate_speech": "0",
    "date": "1803-11-22",
    "errata": "11",
    "entity": "False",
    "debate_file": "S1V0001P0",
    "debate_name": "[COMMITTEE OF PRIVILEGES]—",
    "debate_text": "moved that Lord Walsingham be appointed chairman of the committee of privileges for the present session.",
    "debate_category": "Uncategorized",
    "debate_speaker": "Lord Hawkesbury",
    "debate_house": "NA",
    "debate_image": "HOUSE OF LORDS.",
    "debate_column": "S1V0001P0I0024",
    "debate_label": "Walsingham",
    "debate_constituency": "PERSON"
  }
]
```

> **Give me all debates for speaker "Lord King"**  
> Note: Use HTML encoding for spaces, that is to say you should replace spaces with %20
>
> [https://api.democracylab.io/hansard/debate?debate_speaker=eq.Lord%20King](https://api.democracylab.io/hansard/debate?debate_speaker=eq.Lord%20King)

> **Pagination: It is advisable to paginate if you are expecting more than, say, 100 results**
>
> The first 5 debates (1-5):  
> [https://api.democracylab.io/hansard/debate?limit=5&offset=0](https://api.democracylab.io/hansard/debate?limit=5&offset=0)
>
> The next 5 (5-10):  
> [https://api.democracylab.io/hansard/debate?limit=5&offset=5](https://api.democracylab.io/hansard/debate?limit=5&offset=5)
