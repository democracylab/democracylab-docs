---
id: database
title: Hansard Dataset
sidebar_label: Hansard Dataset
---

# ER Diagram

![er](https://assets.democracylab.io/img/hansard_debate_er.png)

The Hansard dataset is available as [an API](api) which you can use for your own work.

This database, and the API which it drives, were built from the XML files provided by the [official UK Hansard site](https://hansard.parliament.uk), extensively cleaned via a set of scripts. The raw cleaned data is available as a single large tab delimited file with the headers defined below.
You may aquire a copy of this via M2 at SMU.

# Import Fields

> **FileID:** (API: `file_id` / text)  
> This refers to the name of the source xml file. It is comprised of series number and volume number and P0. It is not an image number, however all source images contain this information.

> **SectionID:** (API: `debate_section` / integer)  
> This refers to each debate within each DebateID. They count up chronologically as they appeared in the source xml.

> **SentenceID:** (API: `debate_sentence` / integer)  
> This refers to each sentence within each debate. They count up and reset whenever the SectionID changes. As we refine the sentence extraction process, these numbers will change, but the structure will remain the same.

> **Speech_ID:** (API: `debate_speech` / integer)  
> This refers to each sentence within each debate. They count up and reset whenever the SectionID changes. As we refine the sentence extraction process, these numbers will change, but the structure will remain the same.

> **Date:** (API: `debate_date` / text)  
> Date in the format YYYY-MM-DD. This is the date the debate took place. Each row has a unique “address” consisting of DebateID+Date+SectionID+SentenceID.
>
> For example, the Committee of Privileges debate beginning on November 22nd 1803 can be found under:
> S1V0001P0 1803-11-22 0 0 and continues to S1V0001P0 1803-11-22 0 6
>
> The following debate (Parochial Clergy) can then be found under: S1V0001P0 1803-11-22 1 0 and continues to S1V0001P0 1803-11-22 1 2
>
> Note: This can cause some cases where a debate occurred over multiple days and thus will have a different date and the SectionID and SentenceID will reset. This is something that could possibly be improved upon in future iterations.

> **Debate:** (API: `debate_name` / text)  
> The title of the debate

> **Category:** (API: `debate_category` / text)
> This captures if a debate title is denoted as a Question, Minute, or Errata

> **Text:** (API: `debate_text` / text)  
> Transcript of the text, from the source xml. Some rudimentary cleaning has been done to this point.
> Broken up by sentences.

> **Speaker Constituency:** (API: `debate_constituency` / text)  
> What place the speaker represents. Most of this is probably blank right now and will be filled in as we get more metadata on speakers/constituencies/posts and years. (Steph is in charge of this) However, if it is tagged in the XML then it is in the table.

> **House:** (API: `debate_house` / text)  
> House of Commons or House of Lords

> **Image:** (API: `debate_image` / text)  
> In the xml files, there are tags that denote the source image for the content. This has been captured here.

> **Column:** (API: `debate_column` / integer)  
> Similarly to Image, each image has 2 columns, and the column reference is captured here

> **Errata:** (API: `column_id` / text)  
> If the text or speech title contains errata, this is denoted as True, otherwise it is False (the vast majority)

> **Ententies (sic):** (API: `entity` / text)  
> If the text or speech title contains errata, this is denoted as True, otherwise it is False (the vast majority)

> **Labels:** (API: `debate_label` / text)  
> ?

---
