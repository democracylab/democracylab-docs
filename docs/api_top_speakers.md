---
id: api_top_speakers
title: Hansard API
sidebar_label: Hansard API
---
As usual there's more than one way to attack this problem. Since we are already starting with the "member_names_mispelled_words" and we have this in the API already, we could query this and use our existing code. That doesn't leverage any pre-computing power though, so it would be cool if we could at least do the counts ahead of time. The following code does that in SQL:

```SQL
SELECT debate_date, 
speaker_name, 
speech_act, 
array_length(regexp_split_to_array(speech_act, '\s'),1) AS number_of_words
FROM api.member_names_mispelled_words
```

But it's slow - it needs to scan all 68M rows, it took me 36 minutes to run, and we still want to rank it after we get the counts, so this won't work for our script.

Let's revisit the materialized view which represents "member_names_mispelled_words," which looks like this:

```SQL
CREATE MATERIALIZED VIEW api.member_names_mispelled_words AS
SELECT
	debate_date,
	debate_name.name AS debate_name,
	debate_speaker.name AS speaker_name,
	debate_text.fulltext AS speech_act
FROM private.debate
	JOIN private.debate_name ON debate_name.id = debate.debate_name
	JOIN private.debate_speaker ON debate_speaker.id = debate.debate_speaker
	JOIN private.debate_text ON debate_text.id = debate.debate_text
```

We definitely need one change, which is to put the counts in the view. While we're there let's do make a second change which will make our life easier later - we'll convert that date which is stored as a string into a real date object, which will allow us do date sorts more efficiently. So we arrive here:

```SQL
CREATE MATERIALIZED VIEW api.member_names_mispelled_words AS
SELECT
	debate_date::timestamptz,
	debate_name.name AS debate_name,
	debate_speaker.name AS speaker_name,
	debate_text.fulltext AS speech_act,
	array_length(regexp_split_to_array(debate_text.fulltext, '\s'),1) AS number_of_words 
FROM private.debate
	JOIN private.debate_name ON debate_name.id = debate.debate_name
	JOIN private.debate_speaker ON debate_speaker.id = debate.debate_speaker
	JOIN private.debate_text ON debate_text.id = debate.debate_text;
```

This takes about 36 min to run initially (as before), and then we need to add indexes, but we made it a materialize view, which means it will be stored for quick lookup, so after it runs we can do the following relatively quickly. This will make a materialized view of  the number of words per speaker, per year: 

```SQL
CREATE MATERIALIZED VIEW api.top_speakers AS
SELECT extract(year FROM debate_date::TIMESTAMP) as date_year, 
speaker_name, 
sum(number_of_words) FROM api.member_names_mispelled_words
GROUP BY date_year, speaker_name
ORDER BY date_year;
```

And we'll make a second one for convenience that just returns the top speaker (we could use the query language on the above to scope, but this makes life a bit easier):
```SQL
DROP  VIEW IF EXISTS api.top_speaker_per_year;
CREATE  VIEW api.top_speaker_per_year AS
SELECT DISTINCT ON (year)
       year, words_spoken, speaker_name
FROM   api.top_speakers
ORDER  BY year, words_spoken DESC, speaker_name;
```

Which means, finally, we can get to it like this:

>All speakers as the above query, as date, speakername, number of words:  
>https://api.democracylab.io/hansard/top_speakers
>
>Ranked order of speakers for a given year:  
>https://api.democracylab.io/hansard/top_speakers?year=eq.1916&order=words_spoken.desc
>
>Only the max speakers for each year:  
>https://api.democracylab.io/hansard/top_speaker_per_year
>
>Only the top speaker for a given year:  
>https://api.democracylab.io/hansard/top_speaker_per_year?year=eq.1935
>
>Top speakers for a range:  
>https://api.democracylab.io/hansard/top_speaker_per_year?year=gt.1916&year=lt.1920
