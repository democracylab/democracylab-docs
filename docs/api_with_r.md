---
id: api_with_r
title: How to use the database/API with R
sidebar_label: How to use the database/API with R
---

## Goal: Produce a visualization of which can be updated in close to realtime.

1. Pre-compute the dataset using a query (runs in >500ms, updated live)

   ```
   SELECT count(debate.id) AS number_of_debates, private.debate_speaker.name FROM private.debate
   INNER JOIN private.debate_speaker ON debate_speaker.id = debate.speaker_id GROUP BY debate_speaker.name
   ORDER BY number_of_debates DESC;
   ```

2. Make this available as an API endpoint:

   ```
   CREATE VIEW api.debate_count_by_speaker AS

   SELECT count(debate.id) AS number_of_debates, private.debate_speaker.name FROM private.debate
   INNER JOIN private.debate_speaker ON debate_speaker.id = debate.speaker_id GROUP BY debate_speaker.name
   ORDER BY number_of_debates DESC;

   GRANT SELECT ON  api.debate_count_by_speaker TO web_anon;
   ```

3. Access this data via R:
   Note: There are several libraries available for working with JSON data in R, [this blog post](http://blog.rolffredheim.com/2014/03/better-handling-of-json-data-in-r.html) reviews them. Here we use `jsonlite`

   ```
   install.packages("jsonlite")
   install.packages('curl')
   data <- fromJSON("https://api.democracylab.io/hansard/debate_count_by_speaker")
   ```

4. At this point you can do anything you can do with R/Shiny. As an example, here is a simple barchart:

   `barplot(data$number_of_debates, names.arg = data$name, las=2, width=30, xlim=c(1, length(data$number_of_debates) ), cex.names=.7)`

   ![barplot](https://assets.democracylab.io/img/barplot.png)
