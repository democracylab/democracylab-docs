---
id: vpn_setup
title: VPN Access
sidebar_label: VPN: Windows
---

**Please note that this information is for the DemocracyLab development team, access to the VPN will NOT be granted to anyone else. Public access is available via the [API](api)**.

> This VPN uses a 'split tunnel' configuration, meaning that it will route only traffic intended for democracylab.io and transparentparliament.com via the VPN, all other traffic will use your normal internet connection.

The backend of this system, including the database which powers the project, is only directly accessible via a VPN. This is by design: all of the information will be available via API without login credentials, however there are some special cases where direct access is needed by members of the development team.

You will need:

- A VPN Client (see below)
- A profile file `democracylab.ovpn` (see Andrew)
- A passphrase (see Andrew)

# OSX

1. Download and install [Tunnelblick Stable](https://tunnelblick.net/downloads.html)

2. Double click on the `democracaylab.ovpn` file to install the profile.

3. Enter the passphrase when prompted.

4. To use: Tunnelblick lives in the mac menu bar. From the tunnelblick icon, right click and select "connect to democracylab"

5. Test the connection by visiting https://democracylab.io:9000/. You should see a login screen. If it times out, the VPN is not working.

# Windows 10

1. Download the [OPENVPN Client for Windows 10](https://openvpn.net/community-downloads/) here.

2. Install OPENVPN and do not change any option or setting.
3. Start OPENVPN and you will receive an error message telling you that it is missing
   a configuration file. This is normal and you can ignore this error.
4. Locate in your taskbar an icon with a screen with a lock in it.
5. Right-click and select "Settings".
6. Choose `democracaylab.ovpn` file and click OK.
7. Right-click the icon again and select "Connect".
8. You will now be asked for a password. Submit the password which has been provided to you.
9. There will be warnings in the log panel which are safe to ignore. If all goes well, the panel will minimize and you will see the message "Democracylab is now connected"
10. Test the connection by visiting https://democracylab.io:9000/. You should see a login screen. If it times out, the VPN is not working.

# Accessing Postgres

Once the VPN is running, the database may be accessed using any client you wish including the standard Postgres `psql` CLI tool.

For OSX I recommend [Postico](https://eggerapps.at/postico/). For Windows users [a list of clients is avaialable here](https://wiki.postgresql.org/wiki/PostgreSQL_Clients).

> **Credentials:**
>
> _host:_ database.democracylab.io  
> _port:_ 5432  
> _user:_ postgres  
> _database:_ postgres  
> _password:_ (see andrew)

# Accessing Servers

Once the VPN is running, the servers `database.democracylab.io`, `vpn.democracylab.io` and `cloud.democracylab.io` may all be accessed via SSH. Please [prepare your SSH keys](https://confluence.atlassian.com/bitbucketserver/creating-ssh-keys-776639788.html) and speak to Andrew to get access.
